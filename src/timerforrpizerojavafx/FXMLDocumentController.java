/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timerforrpizerojavafx;

import Settings.DataSettings;
import Settings.IOSettings;
import debug.CursorCircle;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import mainDo.Controls;
import mainDo.main_do;
import sys.TypeSystem;
import timerWork.timerForRun;
import java.lang.Thread;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author Slava
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private ProgressBar ProgressBarState;  
    
    @FXML
    private Button buttonSettings;
    
    @FXML
    private Button buttonStart;
    
    @FXML
    private Button buttonStop;
    
  
    @FXML 
    private AnchorPane anchorPane;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        //label.setText("Hello World!");

        //https://docs.oracle.com/javafx/2/text/jfxpub-text.htm

        /*
        https://wiki.debian.org/Fonts
        
        $ sudo cp '/home/pi/digital-7(mono).ttf' '/usr/share/fonts/'
        $ sudo fc-cache -fv

        cp *.otf ~/.fonts/  #copy
        cp *.ttf ~/.fonts/  #copy
        fc-cache -v -f      #install
        
        sudo fc-list        #print
        */
        
        //Font value = Font.loadFont(FXMLDocumentController.class.getResource("digital-7.ttf").toExternalForm(), 60);//digital-7.ttf

        //Path fontPath = Paths.get("/resources/fonts/digital-7.ttf");
        //Path currentRelativePath = Paths.get("");
        //String s = currentRelativePath.toAbsolutePath().toString();
        //System.out.println("Current relative path is: " + s);
        
        Font value = null;
        //value = Font.loadFont(Files.newInputStream(fontPath), 70);
        
        //value = Font.loadFont(getClass().getClassLoader().getResource("/resources/fonts/digital-7.ttf").toExternalForm(), 80);
        //String relName = "digital-7.ttf";
        //InputStream is = FXMLDocumentController.class.getResourceAsStream(relName);
        String url = this.getClass().getResource("").getPath();
        System.out.println(url);
        
        url = this.getClass().getResource("/resources/fonts/d7.ttf").getPath();
        System.out.println(url);        

        url = this.getClass().getResource("/resources/fonts/").getPath();
        System.out.println(url);          
        
        String relName = "/resources/fonts/d7.ttf";//digital-7_mono.ttf
        
        System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
       
        //InputStream stream = new ClassPathResource("package/resource").getInputStream();
        
        InputStream is = FXMLDocumentController.class.getResourceAsStream(relName);
        
        //file:/home/pi/
        
        if(is != null) System.out.println(is.toString());
        
        //is = FXMLDocumentController.class.getClassLoader().getResourceAsStream(relName);
        
        //System.out.println(is.toString());
        
        //value = Font.loadFont("/resources/fonts/digital-7.ttf", 100);
        value = Font.loadFont(is, 100);
        System.out.println("value " + value);
        //System.out.println(getClass().getClassLoader().toString());
        //System.out.println(getClass().getClassLoader().getResource("/resources/fonts/digital-7.ttf").toString());
        
        if(value == null)
            value = Font.font("Digital-7 Mono", 100);//жагружаем как системный (должен быть установлен в системе , список шрифтов системы Font.getFamilies())
        //digital-7 Mono
        // ISOCPEUR
        //value = Font.font("ISOCPEUR", 64);
        label.setText("123");
        
        String s = label.getFont().getFamily() + "  " + label.getFont().getName() + "  " + label.getFont().getStyle();
        System.out.println(s);
        
        //label.setFont(Font.font("Monospaced Bold Italic", 60));
        
        label.setTextFill(Color.GREEN);
        
        if(value != null)
        {
         s = value.getFamily() + "  " + value.getName() + "  " + value.getStyle();
         System.out.println(s);
         label.setFont(value);
        
        }
        
        
        s = label.getFont().getFamily() + "  " + label.getFont().getName() + "  " + label.getFont().getStyle();
        System.out.println(s);
        
        //for(String i : Font.getFamilies())
        //{
            //System.out.println(i);
        //}
        //System.out.println("  ");
        /*
        DejaVu Sans
        DejaVu Sans Mono
        DejaVu Serif
        Digital-7 Mono
        Monospaced
        Quicksand
        Quicksand Light
        Quicksand Medium
        SansSerif
        Serif
        System
        
        */
       
        //for(String i : Font.getFontNames())
        //{
            //System.out.println(i);
        //}
        
        /*
        DejaVu Sans
        DejaVu Sans Bold
        DejaVu Sans Mono
        DejaVu Sans Mono Bold
        DejaVu Serif
        DejaVu Serif Bold
        Digital-7 Mono
        Monospaced Bold
        Monospaced Bold Italic
        Monospaced Italic
        Monospaced Regular
        Quicksand Bold
        Quicksand Light
        Quicksand Medium
        Quicksand Regular
        SansSerif Bold
        SansSerif Bold Italic
        SansSerif Italic
        SansSerif Regular
        Serif Bold
        Serif Bold Italic
        Serif Italic
        Serif Regular
        System Bold
        System Bold Italic
        System Italic
        System Regular
        */

       
        relName = "/resources/fonts/isocpeur.ttf";
        is = FXMLDocumentController.class.getResourceAsStream(relName);

        System.out.println(is.toString());
        
        value = Font.loadFont(is, 100);
        System.out.println(value.getFamily());

        
        relName = "/resources/fonts/digital-7_mono.ttf";
        is = FXMLDocumentController.class.getResourceAsStream(relName);

        System.out.println(is.toString());
        
        value = Font.loadFont(is, 100);
        
        System.out.println(value.getFamily());
        
        //System.exit(0);
    }
    
    
    private void LoadFont()
    {
	Font value = null;
	String relName;
	InputStream is;
        
        relName = "/resources/fonts/digital-7_mono.ttf";
        is = FXMLDocumentController.class.getResourceAsStream(relName);

        if(is != null) 
        {
            System.out.println(is.toString());
            value = Font.loadFont(is, 230);
        }
        
        if(value == null)
            value = Font.font("Digital-7 Mono", 230);//жагружаем как системный (должен быть установлен в системе , список шрифтов системы Font.getFamilies())
        
        if(is != null)
        { 
            label.setFont(value);
            System.out.println("label.setFont");
        }
    }
    
    private int CurtentSecond = 0;
    
    
    //----------------------------------------------------------------------//
    //debug
    
    private Pane RootPane = anchorPane;
    
    private Circle cursorCircle;
    
    EventHandler<MouseEvent> getME()
    {
        return new EventHandler<MouseEvent>() {
                    @Override 
                    public void handle(MouseEvent event)
                    {
                        double x = event.getSceneX();
                        double y = event.getSceneY();

                        //event.getSceneX()
                        //cursorCircle.setLayoutX(x);
                        //cursorCircle.setLayoutY(y);

                        cursorCircle.setLayoutX(x);
                        cursorCircle.setLayoutY(y);

                        System.out.println("Scene x = " + x +" Scene y = " + y);
                    }
                };
    }
    
    private void debugCursorCircle()
    {
        Circle cursorCircleRed = new Circle(10.0, 10.0, 8.0, Color.RED); 
        cursorCircle = cursorCircleRed;
        
        //Parent p = (Parent)anchorPane;
        //ObservableList<Node> lsp = p.getChildrenUnmodifiable();
        //Scene scene = ((Node)anchorPane).getScene();
        //Node parentNode = scene.getRoot();
        
        RootPane.setOnMouseMoved(getME());
        
        //parentNode.get 
        ObservableList<Node> ls = RootPane.getChildren();
        
        //ls.add((Node)anchorPane);
    
        for(Node n : ls)
        {
            if(!n.isMouseTransparent())
            {        
                n.setOnMouseMoved(getME());
            } 
        }
        

        RootPane.getChildren().add(cursorCircleRed);

    }
    
    //----------------------------------------------------------------------//
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
        //RootPane = anchorPane;
        //debug
        //debugCursorCircle();
        CursorCircle debugCursorCircle = new CursorCircle(anchorPane);
        
        // TODO
        dothis();

        //LoadFont();
        
        int m = TypeSystem.AllSeconds/60;
        int s = TypeSystem.AllSeconds%60;
        
        String time = String.format("%02d", m) + ":" + String.format("%02d", s);
        
        label.setText(time);
        Font value = Font.font("Digital-7 Mono", 230);//жагружаем как системный (должен быть установлен в системе , список шрифтов системы Font.getFamilies())
        
        if(value != null)
        { 
            label.setFont(value);
        }
        
        buttonStop.setDisable(true);
        //----------------------------------------------//
        
        ProgressBarState.setProgress(0.0d);//0-1 
        
        // 0-min TypeSystem.AllSeconds-max
        //x - CurtentSecond    
        //x = CurtentSecond / TypeSystem.AllSeconds
        
        //debug ------------------------------//
        //threadCursor();
        
    }    
    
    public void handleButtonActionStart(ActionEvent event)
    {
            timerStart();
    }
 
    public void handleButtonActionStop(ActionEvent event)
    {
            timerStop();
    }
        
    private void setProgressBarState(int timeSec)
    {
        double t = (double)timeSec / (double)TypeSystem.AllSeconds;
        ProgressBarState.setProgress(t);
        //System.out.println(t);
    }
    
    private void setLableState(int timeSec)
    {
        int m = timeSec/60;
        int s = timeSec%60;
        
        String time = String.format("%02d", m) + ":" + String.format("%02d", s);
        
        label.setText(time);
    }
    
    private static main_do md;
    public void set_main_do(main_do md)
    {
        this.md = md;
    }
    
    private timerForRun tfr;
    private void timerStart()
    {
        //LoadFont();
        buttonSettings.setDisable(true);
    
        buttonStart.setDisable(true);
    
        buttonStop.setDisable(false);
        
        //buttonStop;
        
        CurtentSecond = TypeSystem.AllSeconds;
        tfr = new timerForRun(new Runnable() {
            @Override
            public void run() {
                 timerDo();
            }
        });
        
        md.pinWork.setOutPin(true);

    }
    
    private void timerDo()
    {
        CurtentSecond--;
        if(CurtentSecond < 0)
        {
            timerStop();
            return;
        }        
        Platform.runLater(() -> {
            setProgressBarState(TypeSystem.AllSeconds - CurtentSecond);
            setLableState(CurtentSecond);
            //System.out.println(currentTimeMillis);
        });
        

    }
    
    
    //debug ------------------------------//
    private void debugCursor()
    {
        double x;
        double y;        
        //System.out.println("x = " + x +" y = " + y);
        
        //PointerInfo a = MouseInfo.getPointerInfo();
        //Point b = a.getLocation();
        //x = b.getX();
        //y = b.getY();
        
        //anchorPane
        com.sun.glass.ui.Robot robot;        
        robot = com.sun.glass.ui.Application.GetApplication().createRobot();

        x = robot.getMouseX(); 
        y = robot.getMouseY();

        double dx = TypeSystem.getLocationWindow().getX();
        double dy = TypeSystem.getLocationWindow().getY();
        
        x = x - dx;
        y = y - dy;
        System.out.println("x = " + x +" y = " + y);
        
        cursorCircle.setLayoutX(x);
        cursorCircle.setLayoutY(y);
        
        //cursorCircle.setVisible(true);


    }
    
    private boolean threadCursorRun = false;
    private void threadCursor()
    {    
        threadCursorRun = true;
        Thread t;
        t = new Thread(() -> { 
            while(true)
            {
                if(!threadCursorRun) return;
                debugCursor();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }
    
    //debug ------------------------------//
    
    
    private void timerStop()
    {
        md.pinWork.setOutPin(false);
        
        if(tfr != null) tfr.timerStop();
        Platform.runLater(() -> {
            setProgressBarState(0);
            setLableState(TypeSystem.AllSeconds);
            
            
            buttonSettings.setDisable(false);
    
            buttonStart.setDisable(false);
        
            buttonStop.setDisable(true);
            
            System.out.println("timerStop");
        });
        

    }
    
    
    
    public void changeScreenButtonPushed(ActionEvent event) throws IOException
    {
        threadCursorRun = false;
        
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("SettingsFXML.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        
        //This line gets the Stage information
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(tableViewScene);
        window.show();
    }
        
    
    public void dothis()
    {
//       Controls controls = new Controls();
//       if(md==null)
//            md = new main_do(controls);
    }
    
    public void ExitFromController()
    {
       if(md!=null) md.stop(); 
    }
    
    
    public void getOnMouseMoved(MouseEvent event)
    {
//                double x = event.getX();
//                double y = event.getY();
//
//                cursorCircle.setLayoutX(x);
//                cursorCircle.setLayoutY(y);
//
//                System.out.println("x = " + x +" y = " + y);
    }
    
    

}

