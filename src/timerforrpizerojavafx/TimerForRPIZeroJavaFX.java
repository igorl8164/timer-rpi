/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timerforrpizerojavafx;

import Settings.DataSettings;
import Settings.IOSettings;
import debug.CursorCircle;
import java.awt.Point;
import java.io.InputStream;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import mainDo.Controls;
import mainDo.main_do;
import sys.TypeSystem;

/**
 *
 * @author Slava
 */
public class TimerForRPIZeroJavaFX extends Application {
    
    private static FXMLLoader loader;
    
    private static Controls controls = new Controls();   
    private static main_do md=new main_do(controls);;
    
    @Override
    public void start(Stage stage) throws Exception {
        
          
        //LoadFont();
        TypeSystem.isRaspberry();
        
        //read time
        DataSettings ds = IOSettings.ReadDataSettings();
        TypeSystem.AllSeconds = ds.AllTimeSecond;
        
        loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml")); //SettingsFXML.fxml  FXMLDocument.fxml
        Parent root = (Parent)loader.load();
        Scene scene = new Scene(root);
        
        
        scene.setCursor(Cursor.HAND);
//        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
//            @Override 
//            public void handle(MouseEvent event)
//            {
//                double x = event.getX();
//                double y = event.getY();
//
//                //cursorCircle.setLayoutX(x);
//                //cursorCircle.setLayoutY(y);
//
//                System.out.println("x = " + x +" y = " + y);
//            }
//        });
          
        
        
        stage.setScene(scene);
        stage.show();
        
        
        System.out.println("x poz= " + stage.getX() +" y poz= " + stage.getY());
        TypeSystem.setLocationWindow(new Point((int)stage.getX(), (int)stage.getY()));        

        
        FXMLDocumentController controller = (FXMLDocumentController)loader.getController();
        controller.dothis();
        controls.setMainStage(stage);
        controller.set_main_do(md);
        
        System.out.println( "start");
    }

    
    
    @Override
    public void stop() throws Exception {
        super.stop(); //To change body of generated methods, choose Tools | Templates.
        
        FXMLDocumentController controller = (FXMLDocumentController)loader.getController();
        controller.ExitFromController();
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        if(args != null) 
            for(String s : args)
            {
                System.out.println("args[]  " + s); //debug
                if(s.contains("debug"))
                {
                    CursorCircle.setDebug();
                }
            }
            
        
        launch(args);
   

    }
    
    
    private void LoadFont()
    {
	Font value = null;
	String relName, s;
	InputStream is;
        
        relName = "/resources/fonts/digital-7_mono.ttf";
        is = FXMLDocumentController.class.getResourceAsStream(relName);
        if(is != null)
        {
            System.out.println(is.toString()); 
            value = Font.loadFont(is, 100);
        }
        
        if(value == null)
            value = Font.font("Digital-7 Mono", 100);//жагружаем как системный (должен быть установлен в системе , список шрифтов системы Font.getFamilies())
        
        s = value.getFamily() + "  " + value.getName() + "  " + value.getStyle();
         System.out.println(s);
        
        relName = "/resources/fonts/isocpeur.ttf";
        is = FXMLDocumentController.class.getResourceAsStream(relName);

        if(value != null)
        {
            System.out.println(is.toString());
            value = Font.loadFont(is, 100);
        }
        
        if(value == null)
            value = Font.font("ISOCPEUR", 100);//жагружаем как системный (должен быть установлен в системе , список шрифтов системы Font.getFamilies())
        
        if(value != null)
        {
            s = value.getFamily() + "  " + value.getName() + "  " + value.getStyle();
            System.out.println(s);
        }
        
        
    }
}
