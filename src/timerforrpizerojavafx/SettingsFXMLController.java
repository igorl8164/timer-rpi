/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timerforrpizerojavafx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sys.TypeSystem;
import debug.CursorCircle;

/**
 * FXML Controller class
 *
 * @author Slava
 */
public class SettingsFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Button buttonHome;
    
    @FXML
    private Label timeSecond;
    
    @FXML
    private Label timeMinute;
    
    @FXML 
    private AnchorPane anchorPaneRoot;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        
        CursorCircle debugCursorCircle = new CursorCircle(anchorPaneRoot);
        
        m = TypeSystem.AllSeconds/60;
        s = TypeSystem.AllSeconds%60;
        
        timeMinute.setText(String.format("%02d", m));
        timeSecond.setText(String.format("%02d", s)); 
        //Image image = new Image(getClass().getResourceAsStream("/resources/icons/Apps-Home-icon.png"));
        //buttonHome.setGraphic(new ImageView(image));
        
    }    
    
    public void changeScreenButtonPushed(ActionEvent event) throws IOException
    {
        
        TypeSystem.AllSeconds = m*60 + s;

        
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        
        //This line gets the Stage information
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(tableViewScene);
        window.show();
    }
    
    //------------------------------------------------------------------------//
    private int s = 59;
    
    private Timer timerMinusSecond;
    private TimerTask timerTaskMinusSecond = new TimerTask() {
        @Override
        public void run() {
            s--; 
            System.out.println("second = " + s);
        }
    };
    
    private int duration = 0;

    
    //start timer interval 700
    private static int interval = 600;
    private void StartTimerIntervalMinusSecond(int interv)
    {
        this.interval = interv;
        duration = 0;
        timerTaskMinusSecond = new TimerTask() {
            @Override
            public void run() {
                duration++;
                if(s > 1) s--; 
                System.out.println("second = " + s + "  duration = " + duration);
                Platform.runLater(() -> { timeSecond.setText(String.format("%02d", s)); });
                if(duration == 5)
                {
                    if (interval >= 400)
                    {
                        interval = interval - 200;
                        timerMinusSecond.cancel();
                        timerMinusSecond = null; 
                        StartTimerIntervalMinusSecond(interval);
                    }
                }
            }
        };
        
        //запускаем таймер
        timerMinusSecond = new Timer("Timer decrement second");
        System.out.println("Start Timer decrement interval : " + interval);
        timerMinusSecond.scheduleAtFixedRate(timerTaskMinusSecond, 200, interval);
        
        
    }

    
    public void actionEventButtonMinusSecondReleased(MouseEvent  event)
    {
        
        timerMinusSecond.cancel();
        timerMinusSecond = null;        
                
        if(duration == 0)
        {
            if(s>1) s--;
            timeSecond.setText(String.format("%02d", s));
        }
        
        duration = 0;
        System.out.println("Action Event Button Minus Second Released");
    } 
    
        public void actionEventButtonMinusSecondPressed(MouseEvent  event)
    {
        
        StartTimerIntervalMinusSecond(600);
        
        
        System.out.println("Action Event Button Minus Second Pressed");
    }
        
 //------------------------------------------------------------------------//      
 
     private Timer timerPlusSecond;
    private TimerTask timerTaskPlusSecond;
    
    private void StartTimerIntervalPlusSecond(int interv)
    {
        this.interval = interv;
        duration = 0;
        timerTaskPlusSecond = new TimerTask() {
            @Override
            public void run() {
                duration++;
                if(s < 59) s++; 
                System.out.println("second = " + s + "  duration = " + duration);
                Platform.runLater(() -> { timeSecond.setText(String.format("%02d", s)); });
                if(duration == 5)
                {
                    if (interval >= 400)
                    {
                        interval = interval - 200;
                        timerPlusSecond.cancel();
                        timerPlusSecond = null; 
                        StartTimerIntervalPlusSecond(interval);
                    }
                }
            }
        };
        
        //запускаем таймер
        timerPlusSecond = new Timer("Timer increment second");
        System.out.println("Start Timer incriment interval : " + interval);
        timerPlusSecond.scheduleAtFixedRate(timerTaskPlusSecond, 200, interval);
        
        
    }

    
    public void actionEventButtonPlusSecondReleased(MouseEvent  event)
    {
        
        timerPlusSecond.cancel();
        timerPlusSecond = null;        
                
        if(duration == 0)
        {
            if(s<59) s++;
            timeSecond.setText(String.format("%02d", s));
        }
        
        duration = 0;
        System.out.println("Action Event Button Plus Second Released");
    } 
    
        public void actionEventButtonPlusSecondPressed(MouseEvent  event)
    {
        
        StartTimerIntervalPlusSecond(600);
        
        
        System.out.println("Action Event Button Plus Second Pressed");
    }      
        
 //------------------------------------------------------------------------//
    private int m = 05; //0-99
    
    private Timer timerMinusMinute;
    private TimerTask timerTaskMinusMinute;    
    
    private void StartTimerIntervalMinusMinute(int interv)
    {
        this.interval = interv;
        duration = 0;
        timerTaskMinusMinute = new TimerTask() {
            @Override
            public void run() {
                duration++;
                if(m > 0) m--; 
                System.out.println("minute = " + m + "  duration = " + duration);
                Platform.runLater(() -> { timeMinute.setText(String.format("%02d", m)); });
                if(duration == 5)
                {
                    if (interval >= 400)
                    {
                        interval = interval - 200;
                        timerMinusMinute.cancel();
                        timerMinusMinute = null; 
                        StartTimerIntervalMinusMinute(interval);
                    }
                }
            }
        };
        
        //запускаем таймер
        timerMinusMinute = new Timer("Timer decrement minute");
        System.out.println("Start Timer decrement interval : " + interval);
        timerMinusMinute.scheduleAtFixedRate(timerTaskMinusMinute, 200, interval);
        
    }
    
    public void actionEventButtonMinusMinuteReleased(MouseEvent  event)
    {        
        timerMinusMinute.cancel();
        timerMinusMinute = null;        
                
        if(duration == 0)
        {
            if(m>0) m--;
            timeMinute.setText(String.format("%02d", m));
        }
        
        duration = 0;
        System.out.println("Action Event Button Minus Minute Released");
    }
    
    public void actionEventButtonMinusMinutePressed(MouseEvent  event)
    {
        
        StartTimerIntervalMinusMinute(600);
        
        
        System.out.println("Action Event Button Minus Minute Pressed");
    }
   
    
    //-----------------------------------------------------------------------//
    private Timer timerPlusMinute;
    private TimerTask timerTaskPlusMinute;    

    private void StartTimerIntervalPlusMinute(int interv)
    {
        this.interval = interv;
        duration = 0;
        timerTaskPlusMinute = new TimerTask() {
            @Override
            public void run() {
                duration++;
                if(m < 99) m++; 
                System.out.println("minute = " + m + "  duration = " + duration);
                Platform.runLater(() -> { timeMinute.setText(String.format("%02d", m)); });
                if(duration == 5)
                {
                    if (interval >= 400)
                    {
                        interval = interval - 200;
                        timerPlusMinute.cancel();
                        timerPlusMinute = null; 
                        StartTimerIntervalPlusMinute(interval);
                    }
                }
            }
        };
        
        //запускаем таймер
        timerPlusMinute = new Timer("Timer increment minute");
        System.out.println("Start Timer increment interval : " + interval);
        timerPlusMinute.scheduleAtFixedRate(timerTaskPlusMinute, 200, interval);
        
    }
    
    public void actionEventButtonPlusMinuteReleased(MouseEvent  event)
    {        
        timerPlusMinute.cancel();
        timerPlusMinute = null;        
                
        if(duration == 0)
        {
            if(m<99) m++;
            	timeMinute.setText(String.format("%02d", m));
        }
        
        duration = 0;
        System.out.println("Action Event Button Plus Minute Released");
    }
    
    public void actionEventButtonPlusMinutePressed(MouseEvent  event)
    {
        
        StartTimerIntervalPlusMinute(600);
        
        
        System.out.println("Action Event Button Plus Minute Pressed");
    }
    
    
    
}
