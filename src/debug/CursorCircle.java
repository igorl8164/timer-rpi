/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package debug;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Slava
 */
public class CursorCircle {
    
    private static boolean isDebug = false;
    public static void setDebug()
    {
        isDebug = true;
    }
    
    
    public CursorCircle(Pane paneRoot)
    {
        if(isDebug)
        {
            RootPane = paneRoot;
            debugCursorCircle();
        }
    }
    
    private Pane RootPane;
    
    private Circle cursorCircle;
    
    EventHandler<MouseEvent> getME()
    {
        return new EventHandler<MouseEvent>() {
                    @Override 
                    public void handle(MouseEvent event)
                    {
                        double x = event.getSceneX();
                        double y = event.getSceneY();

                        //event.getSceneX()
                        //cursorCircle.setLayoutX(x);
                        //cursorCircle.setLayoutY(y);

                        cursorCircle.setLayoutX(x);
                        cursorCircle.setLayoutY(y);

                        System.out.println("Scene x = " + x +" Scene y = " + y);
                    }
                };
    }
    
    private void listPane(Pane p)
    {
    ObservableList<Node> ls = p.getChildren();
        
        //ls.add((Node)anchorPane);
    
        for(Node n : ls)
        {
            if(!n.isMouseTransparent())
            {        
                n.setOnMouseMoved(getME());
            } 
            
            if(n instanceof Pane) listPane((Pane)n);
        }
    }
    
    private void debugCursorCircle()
    {
        Circle cursorCircleRed = new Circle(10.0, 10.0, 8.0, Color.RED); 
        cursorCircle = cursorCircleRed;
        
        //Parent p = (Parent)anchorPane;
        //ObservableList<Node> lsp = p.getChildrenUnmodifiable();
        //Scene scene = ((Node)anchorPane).getScene();
        //Node parentNode = scene.getRoot();
        
        RootPane.setOnMouseMoved(getME());
        
        //parentNode.get 
        listPane(RootPane);
        

        RootPane.getChildren().add(cursorCircleRed);

    }
}
