/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gpio;

 
import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.*;
import sys.TypeSystem;
//import com.pi4j.io.gpio.GpioController;
//import com.pi4j.io.gpio.GpioFactory;
//import com.pi4j.io.gpio.GpioPin;
//import com.pi4j.io.gpio.GpioPinDigitalInput;
//import com.pi4j.io.gpio.GpioPinDigitalOutput;
//import com.pi4j.io.gpio.PinDirection;
//import com.pi4j.io.gpio.PinMode;
//import com.pi4j.io.gpio.PinPullResistance;
//import com.pi4j.io.gpio.PinState;
//import com.pi4j.io.gpio.RaspiPin;

/**
 *
 * @author Slava
 */
public class PinWork {

// https://pi4j.com/1.2/usage.html
// Usage Pi4J
    
private GpioController gpioController = null;// = GpioFactory.getInstance();
private GpioPinDigitalOutput gpioPinDigitalOutput;// = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_02, "MyLED", PinState.HIGH);
private GpioPinDigitalInput gpioPinDigitalInput;// = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_02,PinPullResistance.PULL_DOWN);
private boolean gpioUse = false; 

public PinWork()
{
    initGPIO();
}

public void initGPIO()
{
    if(!TypeSystem.isRaspberry()) return;
    if(gpioController != null) return;
    //GPIO_02 out
    //GPIO_03 in


    gpioController = GpioFactory.getInstance();
    gpioPinDigitalInput = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_03,PinPullResistance.PULL_UP);
    gpioPinDigitalInput.addListener(new GpioPinListenerDigital() {
       @Override
       public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent gpioPinDigitalStateChangeEvent) {
           System.out.println("GPIO Pin changed" + gpioPinDigitalStateChangeEvent.getPin() + gpioPinDigitalStateChangeEvent.getState());
       }
   });

   gpioPinDigitalOutput = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_02, "Rele", PinState.LOW);
   gpioUse = true;
    //gpioPinDigitalOutput.toggle();
//            Thread.sleep(500);
   System.out.println("Pin inicialization");

}


public void setOutPin(boolean en)
{
    if(!gpioUse) return;
    
    if(en)
    {
        gpioPinDigitalOutput.high();
        System.out.println("setOutPin high");
    }
    else
    {
        gpioPinDigitalOutput.low();
        System.out.println("setOutPin low");
    }
        
}

public boolean getStateInputPin()
{
    if(!gpioUse) return false;
    
    return gpioPinDigitalInput.isLow();
}

public void stopGPIO()
{
   if(!gpioUse) return;
   System.out.println("gpioController.shutdown");
   gpioController.shutdown();
}
    
}
