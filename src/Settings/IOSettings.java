/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Settings;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Slava
 */
public class IOSettings {
    
    //сериализация данных объекта DataSettings
    private static DataSettings dataSettings;

    
    //чтение из файла 
    public static DataSettings ReadDataSettings()
    {
        FileInputStream fis = null;
        String FileName = "DataSettings.out";
        boolean existsFile = false;
        existsFile = (Files.exists(Paths.get(FileName))); 
       
        if(existsFile)    
        {
            System.out.println("file " + FileName + " exists");
            try {
                fis = new FileInputStream(FileName);
                ObjectInputStream oin = new ObjectInputStream(fis);
                dataSettings = (DataSettings) oin.readObject();
                //System.out.println("version="+ts.version);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
                dataSettings = new DataSettings();
            } catch (IOException ex) {
                Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
                dataSettings = new DataSettings();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
                dataSettings = new DataSettings();
            } finally {
                try {
                    if( fis != null) fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else            
        {
            System.out.println("file " + FileName + " not exists");
            dataSettings = new DataSettings();
        }
        return dataSettings;
    }
    
    
    //запись в файл 
    public static void WriteDataSettings(DataSettings ds)
    {
        
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("DataSettings.out", false);//презаписывать файл при повторной записи
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(ds); 
            oos.flush();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(IOSettings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
