/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shutdown;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sys.TypeSystem;

/**
 *
 * @author Slava
 */
public class exit {

    
    public void WindowPowerOff(Stage w)
    {
        try {
            //threadCursorRun = false;
            
            Parent tableViewParent = FXMLLoader.load(getClass().getResource("/shutdown/PowerOff_FXML.fxml"));
            Scene tableViewScene = new Scene(tableViewParent);
            
            //This line gets the Stage information
            
            Stage window = w;//(Stage)((Node)event.getSource()).getScene().getWindow();
            
            window.setScene(tableViewScene);
            window.show();
            
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(exit.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    poweroff();
                }
            }).start();
                        
  
            
        } catch (IOException ex) {
            Logger.getLogger(exit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void poweroff()
    {

        if(!TypeSystem.isRaspberry())
        {
            System.exit(0); 
            return;
        }
                
        try {


            Process process = Runtime.getRuntime().exec("sudo shutdown -h now");//"sudo shutdown -t 5" sudo shutdown -h now  sudo halt && sudo shutdown -t 20
            
        } catch (IOException ex) {
                        Logger.getLogger(exit.class.getName()).log(Level.SEVERE, null, ex);
                   
        }
        System.out.println("Exit");

        System.exit(0);
    }    
}
