/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainDo;

import Settings.DataSettings;
import Settings.IOSettings;
import gpio.PinWork;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.stage.Stage;
import shutdown.exit;
import sys.TypeSystem;
import timerWork.timersec;

/**
 *
 * @author Slava
 */
public class main_do implements Runnable{

    private Controls controls;
    public PinWork pinWork;
    
    private timersec timer_sec;
    
    private boolean PinPowerStatePrevious;
    
    public main_do(Controls controls)
    {
        pinWork = new PinWork();
        //tmp
        pinWork.setOutPin(false);
        PinPowerStatePrevious = pinWork.getStateInputPin();
        this.controls = controls;
        timer_sec = new timersec(this);        
    }

    private Object w = new Object();
    
    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //для таймера раз в секунду        
    Platform.runLater(() -> {
                //allResource.getMainBorderPane().setCenter(allResource.getAnchorPaneLabelLayer());//getAnchorPaneLineChartLayer());
                long currentTimeMillis= System.currentTimeMillis();
                //System.out.println(currentTimeMillis);
                boolean PinPowerState = pinWork.getStateInputPin();
                boolean riseFront=false;
                
                
                //tmp->
                //    exit ext = new exit();
                //    ext.WindowPowerOff(controls.getMainStage());
                //<-tmp    
        
        
                if(PinPowerStatePrevious)
                    if(!PinPowerState) riseFront = true;
                        
                //проверка питания
                if(!riseFront)
                {
                     //System.out.println("PinPowerOn");
                }
                else
                {
                    pinWork.setOutPin(false);
                    pinWork.stopGPIO();
                    //save settings
                    SaveTime();
                    System.out.println("PinPowerOff");
                     
                    exit ex = new exit();
                    
                    //ex.poweroff();
                    ex.WindowPowerOff(controls.getMainStage());
                }
                
                PinPowerStatePrevious = PinPowerState;
                //проверка работы 
            });
           
    }
    
    
    private void SaveTime()
    {
        //save time
        DataSettings ds = new DataSettings();
        ds.AllTimeSecond = TypeSystem.AllSeconds;
        IOSettings.WriteDataSettings(ds);
    }
    
    public void stop()
    {
        pinWork.setOutPin(false);
        pinWork.stopGPIO();
        SaveTime();
        TypeSystem.setRun(false);
        System.out.println("stop timer");
        pinWork.stopGPIO();
    }
}
