/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys;

import java.awt.Point;


/**
 *
 * @author Slava
 */
public class TypeSystem {

    //system run (stop timer off exit)
    private static boolean isRun = false;

    public static boolean isRun() {
        return isRun;
    }

    public static void setRun(boolean isRun) {
        TypeSystem.isRun = isRun;
    }
    //-----------------------------------------------------------------------//
    //process run
    private static boolean ProcessRun = false;

    public static boolean isProcessRun() {
        return ProcessRun;
    }

    public static void setProcessRun(boolean ProcessRun) {
        TypeSystem.ProcessRun = ProcessRun;
    }
    //----------------------------------------------------------------------//
    
    //type system
    private static boolean getArch()
    {
        
        /*
        "OS Architecture : " + System.getProperty("os.arch"));

        "OS Name : " + System.getProperty("os.name"));

        "OS Version : " + System.getProperty("os.version")

        "Data Model : " + System.getProperty("sun.arch.data.model"));
        
        */
        boolean isRasp = false;
        String rez;// = System.getProperty("os.arch");
        //System.out.println("os.arch:  " + rez);
        rez = System.getProperty("os.name");
        //System.out.println(rez);
        if(rez.contains("Linux")) 
        {
            rez = System.getProperty("os.arch");
             
            if(rez.contains("arm"))
            {
                isRasp=true;
                System.out.println("System Raspberry Pi");
            }
        }       
        
        return isRasp;
    }
    

    private static final boolean isRasp = getArch();

    public static boolean isRaspberry()
    {
        return isRasp;
    }
    
    
    
   //-------------------------------------------------------------//
    
    public static int AllSeconds = 0;
    
    
   //-------------------------------------------------------------//
    private static Point locationWindow = new Point();

    public static Point getLocationWindow() {
        return locationWindow;
    }

    public static void setLocationWindow(Point locationWindow) {
        TypeSystem.locationWindow = locationWindow;
    }
    
    
    
}
