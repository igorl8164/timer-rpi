/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timerWork;

import java.util.Timer;
import java.util.TimerTask;
import sys.TypeSystem;

/**
 *
 * @author Slava
 */
public class timerForRun {
    
    
    
private Timer timer;
private Runnable run;
private TimerTask timerTask = new TimerTask()
    {      
        @Override
        public void run()
        {
            timerDo();
            if(!TypeSystem.isRun())
            {
                System.out.println("timer fro run.cancel");
                timer.cancel();
            }
        }
    };


public timerForRun(Runnable run)
{
    this.run = run;
    inicialTimer();
}


    private void inicialTimer()
    {
            if(timer == null)
            {                
                timer = new Timer("Timer One Second For Run");
                //старт таймер
                timer.scheduleAtFixedRate(timerTask, 10, 1000);
            }


    }

    //@Override
    public void timerDo()
    {
        run.run();
        
    //long currentTimeMillis= System.currentTimeMillis();
    //Platform.runLater(() -> {
    //allResource.getMainBorderPane().setCenter(allResource.getAnchorPaneLabelLayer());//getAnchorPaneLineChartLayer());
    //System.out.println(currentTimeMillis);
    //        });

    }

    public void timerStop()
    {
        timer.cancel();
    }
}
